// copyright 2018 calvin scrimshire, all rights reserved.

document.addEventListener("DOMContentLoaded", function(event) { 
	var settings = {
		columns: 2, // integer, range 2-5
		headline_images: false, //boolean
		title: false, // boolean
		reviewer: false, // boolean
		alignment: 'left', // left, right, center, or justify
	}

	// #columnizer_app

	var columns = [ ];

	var frames = {
		settings: '', //static html output
		columns: '', //static html output
		fin: ''
	}
	document.getElementById('clmnzr_form').onsubmit=function() {
		// validate form input
		
		// update settings
		settings.columns = document.forms["clmnzr_form"]["columns"];
		settings.headline_images = document.forms["clmnzr_form"]["headline_images"];
		settings.title = document.forms["clmnzr_form"]["title"];
		settings.reviewer = document.forms["clmnzr_form"]["reviewer"];	
		settings.alignment = document.forms["clmnzr_form"]["alignment"];

		// create column loop output
		for (i=0;i<settings.columns;i++) {
			
		}
		// push user through column ux
		// an example of event delegation
		// ! this could take over the above code, and even change how I'm using html forms vs html generated in js 1st.
		document.getelementbyId('columnizer').addEventListener("submit",function(e){
			e.preventDefault();a
			console.log('submit delegation caught!');
			// validate column submit
			// save vars to memory
			// increment to correct column next
			// or show off finalized code block.
		}
	
		document.getElementById('columnizer').innerHTML = 'hello world';
		return false; // required to prevent form from submitting.
	}

});

